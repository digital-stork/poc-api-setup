import express from "express";

export default class AppRouter {
  private static instance: express.Router;

  static getInstance(): express.Router {
    if (!AppRouter.instance) {
      AppRouter.instance = express.Router();
      return AppRouter.instance;
    }
    return AppRouter.instance;
  }
}

/*import { Router } from 'express';

import controllers from './controllers';
// should be replaced by 'express-marshal' when updated
// @ts-ignore
import {mount} from './utils/marshal'
// @ts-ignore
const router: Router = new Router();
mount(router, controllers);

export default router*/
