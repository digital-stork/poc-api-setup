import pino from 'pino';

import config from '../config/config';

/* istanbul ignore next */
const logger = pino({
  name: config.get('logger').name,
  prettyPrint: { colorize: true },
  level: config.get('logger').level,
});
export default logger;
