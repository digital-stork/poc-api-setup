"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
	for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
	for (var r = Array(s), k = 0, i = 0; i < il; i++)
		for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
			r[k] = a[j];
	return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Joi = require("joi");
var express_1 = require("express");
exports.prettyPath = function (path) {
	if (path[0] !== '/') {
		throw new Error("Path `" + path + "` must begin with a slash.");
	}
	if (path === '/') {
		return path;
	}
	return path.replace(/\/*/, '/').replace(/\/*$/, '');
};
exports.mount = function (router, controllers) {
	controllers.forEach(function (ctrl) {
		if (!ctrl.__router) {
			throw new Error(ctrl.name + " requires a router property. Make sure you decorated your class with @controller(basepath)");
		}
		router.use(ctrl.__router);
	});
};
exports.controller = function (basepath, middleware) {
	if (middleware === void 0) { middleware = []; }
	return function (target) {
		if (!basepath) {
			throw new Error("@controller declaration for `" + target.name + "` is missing a basepath.");
		}
		var router = express_1.Router();
		if (target.__params) {
			target.__params.forEach(function (_a) {
				var id = _a.id, handler = _a.handler;
				router.param(id, function (req, res, next) {
					var rest = [];
					for (var _i = 3; _i < arguments.length; _i++) {
						rest[_i - 3] = arguments[_i];
					}
					Promise.resolve(handler.call.apply(handler, __spreadArrays([target, req, res, next], rest))).catch(next);
				});
			});
		}
		if (target.__routes) {
			target.__routes.forEach(function (route) {
				var handler = target.prototype[route.key];
				router[route.method].apply(router, __spreadArrays([exports.prettyPath(basepath + route.path)], middleware, route.middleware, [function (req, res, next) {
					Promise.resolve(handler.call(target, req, res, next)).catch(next);
				}]));
			});
		}
		target.__router = router;
	};
};
exports.contentType = function (contentType) {
	return function (target, _, descriptor) {
		var fn = descriptor.value;
		descriptor.value = function (req, res) {
			var rest = [];
			for (var _i = 2; _i < arguments.length; _i++) {
				rest[_i - 2] = arguments[_i];
			}
			if (!req.is(contentType)) {
				res
					.status(400)
					.json({ message: "Route requires Content-Type: " + contentType });
				return;
			}
			return fn.apply(target, __spreadArrays([req, res], rest));
		};
	};
};

exports.param = function (id) {
	return function (target, _, descriptor) {
		var params = target.constructor.__params || [];
		target.constructor.__params = __spreadArrays(params, [
			{
				id: id,
				handler: descriptor.value,
			},
		]);
	};
};
exports.route = function (method, path, middleware) {
	if (middleware === void 0) { middleware = []; }
	return function (target, key) {
		var routes = target.constructor.__routes || [];
		target.constructor.__routes = __spreadArrays(routes, [
			{
				method: method,
				path: exports.prettyPath(path),
				middleware: middleware,
				key: key,
			},
		]);
	};
};
var routeFactory = function (method) { return function (path, middleware) {
	return exports.route(method, path, middleware);
}; };
exports.get = routeFactory('get');
exports.post = routeFactory('post');
exports.put = routeFactory('put');
exports.patch = routeFactory('patch');
exports.head = routeFactory('head');
exports.del = routeFactory('delete');
