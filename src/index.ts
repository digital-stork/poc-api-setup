import express from 'express'

import config from './config/config';
import logger from './utils/logger';
import { router } from './decorators/controller';
import './controllers'

const app = express();
app.use(express.json());
app.use(router);

app.listen(config.get('server').port, () => {
  logger.info('We are live on ' + config.get('server').port)
})

export default app
