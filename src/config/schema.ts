const schema = {
  env: {
    doc: 'The API environment.',
    format: ['production', 'staging', 'test', 'development'],
    default: 'development',
    env: 'DS_ENV',
  },
  server: {
    host: {
      doc: 'The IP address to bind.',
      format: 'ipaddress',
      default: '0.0.0.0',
      env: 'DS_SERVER_HOST',
    },
    port: {
      doc: 'The port to bind.',
      format: 'port',
      default: 3002,
      env: 'DS_SERVER_PORT',
    },
    cors: {
      allowedOrigins: {
        doc: 'Server default CORS origin',
        format: Array,
        env: 'DS_DEFAULT_CORS_ORIGINS',
        default: ['*'],
      },
    },
  },
  logger: {
    name: {
      doc: 'API POC',
      format: String,
      default: 'DS_API_POC',
    },
    level: {
      doc: 'Logger level',
      format: ['trace', 'debug', 'info', 'warn', 'error', 'fatal'],
      env: 'DS_API_LOG_LEVEL',
      default: 'trace',
    },
  },
};

export default schema;
