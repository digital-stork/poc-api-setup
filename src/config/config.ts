import convict from 'convict';
import { ipaddress } from 'convict-format-with-validator';

convict.addFormat(ipaddress);

// Define a schema
import schema from './schema';

const config = convict(schema);

// Load environment dependent configuration
const env = config.get('env');
config.loadFile(`src/config/envs/${env}.json`);

// Provides a convict configuration object
export default config;
