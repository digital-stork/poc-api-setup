export interface IContainerProvider {
  useValue: any;
  token: string;
}

export class Container {
  _providers: { [key: string]: any } = {};

  public resolve(token: string) {
    const matchedProviderKey = Object.keys(this._providers).find((key: string) => key === token)
    if (matchedProviderKey) {
      return this._providers[token];
    } else {
      throw new Error(`No provider found for ${token}!`);
    }
  }

  public provide(details: IContainerProvider): void {
    this._providers[details.token] = details.useValue;
  }
}

export const container = new Container();
