import 'reflect-metadata';
import { RequestHandler, Router } from 'express';
import { join } from 'path';

import { HttpMethod } from '../constants/HttpMethods';
import { Metadatas } from '../constants/Metadatas';
import logger from '../utils/logger';

export const router = Router();

export const Controller = (route: string, ...interceptors: RequestHandler[]) => {
  return (constructor: { new (): any }) => {
    const controller = new constructor()
    Object.keys(constructor.prototype).forEach((key) => {
      const handler = controller[key];
      if (typeof handler === 'function' && Reflect.hasMetadata(Metadatas.Method, constructor.prototype, key)) {
        const method: HttpMethod = Reflect.getMetadata(Metadatas.Method, constructor.prototype, key);
        const path: string = Reflect.getMetadata(Metadatas.Path, constructor.prototype, key);
        const fullRoute = join('/', route, path);
        router[method](fullRoute, ...interceptors, handler.bind(controller));
        logger.debug(`${method.toUpperCase()} ${fullRoute} handler created`);
      }
    });
  };
}
