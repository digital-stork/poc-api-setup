import { ObjectSchema } from 'joi';
import 'reflect-metadata';

import { HttpMethods } from '../constants/HttpMethods';
import { Metadatas } from '../constants/Metadatas';

const createMethodHandler = (method: HttpMethods, path: string, schema?: ObjectSchema) => {
  return (target: object, propertyKey: string, descriptor: PropertyDescriptor) => {
    Reflect.defineMetadata(Metadatas.Path, path, target, propertyKey);
    Reflect.defineMetadata(Metadatas.Schema, schema, target, propertyKey);
    Reflect.defineMetadata(Metadatas.Method, method, target, propertyKey);
  };
}

export const Get = (path: string, schema?: ObjectSchema) => createMethodHandler(HttpMethods.GET, path, schema);
export const Patch = (path: string, schema?: ObjectSchema) => createMethodHandler(HttpMethods.PATCH, path, schema);
export const Post = (path: string, schema?: ObjectSchema) => createMethodHandler(HttpMethods.POST, path, schema);
export const Put = (path: string, schema?: ObjectSchema) => createMethodHandler(HttpMethods.PUT, path, schema);
export const Options = (path: string, schema?: ObjectSchema) => createMethodHandler(HttpMethods.OPTIONS, path, schema);
export const Connect = (path: string, schema?: ObjectSchema) => createMethodHandler(HttpMethods.CONNECT, path, schema);
export const Trace = (path: string, schema?: ObjectSchema) => createMethodHandler(HttpMethods.TRACE, path, schema);
export const Head = (path: string, schema?: ObjectSchema) => createMethodHandler(HttpMethods.HEAD, path, schema);
export const Delete = (path: string, schema?: ObjectSchema) => createMethodHandler(HttpMethods.DELETE, path, schema);
