import { NextFunction, Request, Response } from 'express';
import joi from 'joi';
import { BAD_REQUEST } from 'http-status'

export function Validate(schema: Object): MethodDecorator {
  return (
    target: Object,
    propertyKey: string | symbol,
    descriptor: PropertyDescriptor,
  ): PropertyDescriptor | void => {
    const originalMethod = descriptor.value;

    descriptor.value = (req: Request, res: Response, next: NextFunction) => {
      // create schema object
      const joiSchema = joi.object(schema);
      // schema options
      const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: false, // remove unknown props
      };
      // validate request against schema
      const {error, value} = joiSchema.validate(req, options);
      if (error) {
        // on fail return comma separated errors
        res.status(BAD_REQUEST).json({
          error: error.details.map(x => x.message).join(', '),
        });
      } else {
        return originalMethod.apply(target, [value, res, next]);
      }
    };
  };
}
