import { container } from '../instances/container';

export function Injectable(token: string): Function {
  return function(target: { new (): any }): void {
    container._providers[token] = new target();
  };
}
