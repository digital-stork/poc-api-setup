import { Request, Response } from 'express';
import joi from 'joi';

import { Get, Post } from '../decorators/methods';
import { Validate } from '../decorators/validate';
import { Controller } from '../decorators/controller';
import { Inject } from '../decorators/inject';
import { TotoService } from '../services/toto.service';

@Controller('/toto')
export class TotoController {

  @Inject('totoService')
  private _totoService!: TotoService;

  //@Auth({permissions:['admin_read']})
  @Get('/')
  getToto(req: Request, res: Response) {
    this._totoService.sayHello('Hello Toto')
    res.json('Hello Toto');
  }

  @Get('/a/b')
  getTotoWithQueryParam(req: Request, res: Response) {
    this._totoService.sayHello(`Hello Toto: ${req.query.p1}`)
    res.json(`Hello Toto: ${req.query.p1}`);
  }

  @Validate({
    params: {
      id: joi.number(),
    },
  })
  @Get('/:id')
  getTotoById(req: Request, res: Response) {
    this._totoService.sayHello(`Hello Toto: ${req.params.id}`)
    res.json(`Hello Toto: ${req.params.id}`);
  }

  @Validate({
    headers: {
      h1: joi.number().allow(''),
    },
    query: {
      q1: joi.number(),
    },
    params: {
      p1: joi.number().required(),
    },
    body: {
      id: joi.number().required(),
      name: joi.string().required(),
    },
  })
  @Post('/:p1')
  saveToto(req: Request, res: Response) {
    res.json(`Saving ${req.body.name}`);
  }
}
