import { TotoController } from './toto.controller';

// All controllers should be declared here
export const controllers = [
  TotoController,
];

