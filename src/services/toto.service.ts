import { Injectable } from '../decorators/injectable';
import logger from '../utils/logger';

@Injectable('totoService')
export class TotoService {

  public sayHello(message: string): void {
    logger.info(message);
  }
}
