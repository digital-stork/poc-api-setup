import chai, { expect } from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import supertest from 'supertest';
import { OK, BAD_REQUEST } from 'http-status';

import logger from '../src/utils/logger';
import app from '../src';

chai.use(sinonChai);

describe('Toto Controller', () => {

  let loggerInfoSpy: sinon.SinonSpy;
  beforeEach(() => {
    loggerInfoSpy = sinon.spy(logger, 'info');
  });

  afterEach(() => {
    loggerInfoSpy.restore();
  });

  describe('getToto endpoint', () => {
    it('should log "Hello Toto', (done) => {
      supertest(app)
        .get('/toto')
        //.query({ a: 5, b: 'hello world' })
        .set('Content-type', 'application/json')
        .expect(OK)
        .end((err, res) => {
          if (err) {
            throw err;
          }
          expect(loggerInfoSpy).to.be.calledOnce;
          expect(loggerInfoSpy).to.be.calledWith('Hello Toto');
          done();
        });
    });
  });

  describe('getTotoById endpoint', () => {
    it('should log the correct path param', (done) => {
      supertest(app)
        .get('/toto/1')
        .set('Content-type', 'application/json')
        .expect(OK)
        .end((err, res) => {
          if (err) {
            throw err;
          }
          expect(loggerInfoSpy).to.be.calledOnce;
          expect(loggerInfoSpy).to.be.calledWith('Hello Toto: 1');
          expect(res.body).to.eq('Hello Toto: 1');
          done();
        });
    });

    it('should return validation error', (done) => {
      supertest(app)
        .get('/toto/abc')
        .set('Content-type', 'application/json')
        .expect(BAD_REQUEST)
        .end((err, res) => {
          if (err) {
            throw err;
          }
          expect(loggerInfoSpy).not.to.be.called;
          expect(res.body.error).to.eq('"params.id" must be a number');
          done();
        });
    });
  });

  describe('getTotoWithQueryParam endpoint', () => {
    it('should log the correct query param', (done) => {
      supertest(app)
        .get('/toto/a/b')
        .query({p1: 1})
        .set('Content-type', 'application/json')
        .expect(OK)
        .end((err, res) => {
          if (err) {
            throw err;
          }
          expect(loggerInfoSpy).to.be.calledOnce;
          expect(loggerInfoSpy).to.be.calledWith('Hello Toto: 1');
          done();
        });
    });
  });

});

